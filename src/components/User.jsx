import React, {useState} from "react";
import Card from "./Card";

function User(){

  const [isDone, setIsDone] = useState(false);

  function handleClick(){
    setIsDone(!isDone);
  }

  return(
    <h1 onClick={handleClick} style={{textAlign: "center"}}>{!isDone ? "User Account": <Card />}</h1>
  );
}
export default User;
