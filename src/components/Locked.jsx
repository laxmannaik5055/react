import React, {useState} from "react";
import Alert from "./Alert";

function Locked(){
    
    const [done, setDone] = useState(false);

    function handleDone(){
        setDone(!done);
    }

    return(
        <h1 onClick={handleDone}>{!done ? "Locked Secondary Profile": <Alert />}</h1>
    );
}
export default Locked;