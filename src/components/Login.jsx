import React from "react";

function Login(){
    return (
  <div className="container mt-5">
  <h2>Login</h2>

  <div className="row">
    <div className="col-sm-8">
      <div className="card">
        <div className="card-body">

          <form actionName="/login" method="POST">
            <div className="form-group">
              <label for="text">Username</label>
              <input typeName="text" className="form-control" name="username" />
            </div>
            <div className="form-group">
              <label for="password">Password</label>
              <input type="password" className="form-control" name="password" />
            </div>
            <button type="submit" className="btn btn-dark">Login</button>
          </form>

        </div>
      </div>
    </div>



  </div>
</div>
    );
}
export default Login;
