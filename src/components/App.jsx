import React from "react";
import Login from "./Login";
import Register from "./Register";
import User from "./User";
import Locked from "./Locked";

function App(){
    return (
        <div>
        <Login />
        <Register />
        <Locked />
        <User />
        </div>
    );
}
export default App;
