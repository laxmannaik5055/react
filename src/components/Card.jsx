import React from "react";
import "../css/card.css";
import image from "../images/hrithik.jfif";

function Card(){
  return(
    <div>
<h2 className="profile-img"> User Account</h2>

<div className="card">
  <img src={image} alt="John" />
  <h1>Hrithik Roshan</h1>
  <p className="title">Actor</p>
  <p>Mumbai</p>
  <div>
    <a href="#"><i className="fa fa-dribbble"></i></a>
    <a href="#"><i className="fa fa-twitter"></i></a>
    <a href="#"><i className="fa fa-linkedin"></i></a>
    <a href="#"><i className="fa fa-facebook"></i></a>
  </div>
  <p><button>Contact</button></p>
</div>
</div>

  );
}
export default Card;
