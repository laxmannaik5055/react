import React from "react";

function Register(){
    return (
<div className="container mt-5">
  <h2>Register</h2>

  <div className="row">
    <div className="col-sm-8">
      <div className="card">
        <div className="card-body">

          <form actionName="/register" method="POST">
            <div className="form-group">
              <label for="text">Username</label>
              <input type="text" className="form-control" name="username" />
            </div>
            <div className="form-group">
              <label for="password">Password</label>
              <input type="password" className="form-control" name="password" />
            </div>
            <button type="submit" className="btn btn-dark">Register</button>
          </form>

        </div>
      </div>
    </div>



  </div>
</div>
    );
}
export default Register;
